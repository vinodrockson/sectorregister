--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2019-08-01 16:37:50

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE demdb;
--
-- TOC entry 1971 (class 1262 OID 73728)
-- Name: demdb; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE demdb WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';


ALTER DATABASE demdb OWNER TO postgres;

\connect demdb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 1972 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 176 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1974 (class 0 OID 0)
-- Dependencies: 176
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 170 (class 1259 OID 77976)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 171 (class 1259 OID 77978)
-- Name: main_sector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE main_sector (
    id bigint NOT NULL,
    level character varying(255),
    sector_name character varying(255)
);


ALTER TABLE public.main_sector OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 77986)
-- Name: micro_sector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE micro_sector (
    id bigint NOT NULL,
    level character varying(255),
    sector_name character varying(255),
    sub_sector_id bigint
);


ALTER TABLE public.micro_sector OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 77994)
-- Name: register; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE register (
    id bigint NOT NULL,
    has_agreed boolean NOT NULL,
    name character varying(255),
    sector_id character varying(255)
);


ALTER TABLE public.register OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 78002)
-- Name: sub_sector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sub_sector (
    id bigint NOT NULL,
    level character varying(255),
    sector_name character varying(255),
    super_sector_id bigint
);


ALTER TABLE public.sub_sector OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 78010)
-- Name: super_sector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE super_sector (
    id bigint NOT NULL,
    level character varying(255),
    main_sector_id bigint,
    sector_name character varying(255)
);


ALTER TABLE public.super_sector OWNER TO postgres;

--
-- TOC entry 1975 (class 0 OID 0)
-- Dependencies: 170
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


--
-- TOC entry 1962 (class 0 OID 77978)
-- Dependencies: 171
-- Data for Name: main_sector; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO main_sector (id, level, sector_name) VALUES (1, 'one', 'Manufacturing');
INSERT INTO main_sector (id, level, sector_name) VALUES (3, 'one', 'Other');
INSERT INTO main_sector (id, level, sector_name) VALUES (2, 'one', 'Service');


--
-- TOC entry 1963 (class 0 OID 77986)
-- Dependencies: 172
-- Data for Name: micro_sector; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (271, 'four', 'Aluminium and steel workboats', 97);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (269, 'four', 'Boat/Yacht building', 97);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (230, 'four', 'Ship repair and conversion', 97);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (75, 'four', 'CNC-machining', 542);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (62, 'four', 'Forgings, Fasteners', 542);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (69, 'four', 'Gas, Plasma, Laser cutting', 542);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (66, 'four', 'MIG, TIG, Aluminum welding', 542);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (55, 'four', 'Blowing', 559);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (57, 'four', 'Moulding', 559);
INSERT INTO micro_sector (id, level, sector_name, sub_sector_id) VALUES (53, 'four', 'Plastics welding and processing', 559);


--
-- TOC entry 1964 (class 0 OID 77994)
-- Dependencies: 173
-- Data for Name: register; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 1965 (class 0 OID 78002)
-- Dependencies: 174
-- Data for Name: sub_sector; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (342, 'three', 'Bakery and confectionery products', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (43, 'three', 'Beverages', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (42, 'three', 'Fish and fish products', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (40, 'three', 'Meat and meat products', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (39, 'three', 'Milk and dairy products', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (437, 'three', 'Other', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (378, 'three', 'Sweets and snack food', 6);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (389, 'three', 'Bathroom/sauna', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (385, 'three', 'Bedroom', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (390, 'three', 'Children''s room', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (98, 'three', 'Kitchen', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (101, 'three', 'Living room', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (392, 'three', 'Office', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (394, 'three', 'Other (Furniture)', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (341, 'three', 'Outdoor', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (99, 'three', 'Project furniture', 13);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (94, 'three', 'Machinery components', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (91, 'three', 'Machinery equipment/tools', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (224, 'three', 'Manufacture of machinery', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (97, 'three', 'Maritime', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (93, 'three', 'Metal structures', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (508, 'three', 'Other', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (227, 'three', 'Repair and maintenance service', 12);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (67, 'three', 'Construction of metal structures', 11);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (263, 'three', 'Houses and buildings', 11);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (267, 'three', 'Metal products', 11);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (542, 'three', 'Metal works', 11);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (54, 'three', 'Packaging', 9);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (556, 'three', 'Plastic goods', 9);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (559, 'three', 'Plastic processing technology', 9);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (560, 'three', 'Plastic profiles', 9);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (148, 'three', 'Advertising', 5);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (150, 'three', 'Book/Periodicals printing', 5);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (145, 'three', 'Labelling and packaging printing', 5);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (44, 'three', 'Clothing', 7);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (45, 'three', 'Textile', 7);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (337, 'three', 'Other (Wood)', 8);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (51, 'three', 'Wooden building materials', 8);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (47, 'three', 'Wooden houses', 8);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (581, 'three', 'Data processing, Web portals, E-marketing', 28);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (576, 'three', 'Programming, Consultancy', 28);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (121, 'three', 'Software, Hardware', 28);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (122, 'three', 'Telecommunications', 28);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (111, 'three', 'Air', 21);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (114, 'three', 'Rail', 21);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (112, 'three', 'Road', 21);
INSERT INTO sub_sector (id, level, sector_name, super_sector_id) VALUES (113, 'three', 'Water', 21);


--
-- TOC entry 1966 (class 0 OID 78010)
-- Dependencies: 175
-- Data for Name: super_sector; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (19, 'two', 1, 'Construction materials');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (18, 'two', 1, 'Electronics and Optics');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (6, 'two', 1, 'Food and Beverage');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (13, 'two', 1, 'Furniture');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (12, 'two', 1, 'Machinery');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (11, 'two', 1, 'Metalworking');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (9, 'two', 1, 'Plastic and Rubber');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (5, 'two', 1, 'Printing');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (7, 'two', 1, 'Textile and Clothing');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (8, 'two', 1, 'Wood');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (37, 'two', 3, 'Creative industries');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (29, 'two', 3, 'Energy technology');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (33, 'two', 3, 'Environment');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (25, 'two', 2, 'Business services');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (35, 'two', 2, 'Engineering');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (28, 'two', 2, 'Information Technology and Telecommunications');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (22, 'two', 2, 'Tourism');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (141, 'two', 2, 'Translation services');
INSERT INTO super_sector (id, level, main_sector_id, sector_name) VALUES (21, 'two', 2, 'Transport and Logistics');


--
-- TOC entry 1845 (class 2606 OID 77985)
-- Name: main_sector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY main_sector
    ADD CONSTRAINT main_sector_pkey PRIMARY KEY (id);


--
-- TOC entry 1847 (class 2606 OID 77993)
-- Name: micro_sector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY micro_sector
    ADD CONSTRAINT micro_sector_pkey PRIMARY KEY (id);


--
-- TOC entry 1849 (class 2606 OID 78001)
-- Name: register_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY register
    ADD CONSTRAINT register_pkey PRIMARY KEY (id);


--
-- TOC entry 1851 (class 2606 OID 78009)
-- Name: sub_sector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sub_sector
    ADD CONSTRAINT sub_sector_pkey PRIMARY KEY (id);


--
-- TOC entry 1853 (class 2606 OID 78017)
-- Name: super_sector_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY super_sector
    ADD CONSTRAINT super_sector_pkey PRIMARY KEY (id);


--
-- TOC entry 1973 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-08-01 16:37:50

--
-- PostgreSQL database dump complete
--


var app = angular.module('project');

app.controller('RegisterController',function($scope, $http, $route, $location, $timeout) {

	if ($location.path() == "/main"){ 
		$scope.sectors = [];
		$http.get('/rest/sector').success(function(data, status, headers, config) {
			 var refinedList = [];
			 var microList = data.microSectors;
			 var subList = data.subSectors;
			 var superList = data.superSectors;
			 var mainList = data.mainSectors;
			 var mainLevel = " main";		

			 if(app.user_data !== undefined){
				 $scope.regId = app.user_data.id;
				 $scope.name = app.user_data.name 
				 $scope.terms = app.user_data.hasAgreed;
				 $scope.sectorname = app.user_data.sectorId;
			 }
		 
			 
			 angular.forEach(mainList, function (mObj) {
				 mObj.level = mObj.level + mainLevel;
				 refinedList.push(mObj);			 	
				 angular.forEach(superList, function (supObj) {
					 	if(supObj.mainSectorId === mObj.id){
					 		if(subList.findIndex( x => x.superSectorId === supObj.id) > -1){
					 			supObj.level = supObj.level + mainLevel;
					 		}
					 		refinedList.push(supObj);
							 angular.forEach(subList, function (subObj) {
								 	if(subObj.superSectorId == supObj.id){
								 		if(microList.findIndex( x => x.subSectorId === subObj.id) > -1){
								 			subObj.level = subObj.level + mainLevel;
								 		}
								 		refinedList.push(subObj);
										 angular.forEach(microList, function (micObj) {
											 	if(micObj.subSectorId === subObj.id){
											 		refinedList.push(micObj);
											 		}					 	
											 	});
								 		}		
 
								 	}); 
					 		}					 	
					 	});  	
			 	}); 
			 
			 $scope.sectors = refinedList;			

	    });
	 }	
	
	$scope.register = function (isValid) {
		
		var registerId = $scope.regId;
		register = { 
				name: $scope.name,
			    sectorId: $scope.sectorname,
			    hasAgreed: $scope.terms,
			};

		if(isValid){		
			if(isAlpha(register.name))	{
				if(registerId == undefined){
			        $http.post('/rest/register', register).success(function(data, status, headers, config) {
			        	$scope.submitAlert = true;
			         	$scope.submitError = false;
			         	$scope.submitCorrect = true;
			         	$scope.submsg = "Your details have been saved successfully.";
			         	app.user_data = data;
			    	    $timeout(function() {
			        $location.path('/' )
			        }, 2000);     
			    
			        	}).error(function(data, status, headers, config){
			        		$scope.submitAlert = true;
				         	$scope.submitError = true;
				         	$scope.submitCorrect = false;
				         	$scope.submsg = "Technical error. Try again later.";
			         });
				}
				else{
					$http.put('/rest/register/' + registerId, register).success(function(data, status, headers, config) {
			        	$scope.submitAlert = true;
			         	$scope.submitError = false;
			         	$scope.submitCorrect = true;
			         	$scope.submsg = "Your details have been updated successfully.";
			         	app.user_data = data;
			    	    $timeout(function() {
			        $location.path('/' )
			        }, 2000);     
			    
			        	}).error(function(data, status, headers, config){
			        		$scope.submitAlert = true;
				         	$scope.submitError = true;
				         	$scope.submitCorrect = false;
				         	$scope.submsg = "Technical error. Try again later.";
			         });
					
				}
			}
		}
		else{
			
			if(register.name == undefined){
				$scope.nmsg = "Please enter your name.";
				$scope.nameMsg = true;
				$scope.nameError = true;
				$scope.nameCorrect = false;
			} else if(!isAlpha(register.name)){
				$scope.nmsg = "Invalid name!";
				$scope.nameMsg = true;
				$scope.nameError = true;
				$scope.nameCorrect = false;
			}
			
			if(register.sectorId == undefined ){
				$scope.smsg = "Please select at least one sector.";
				$scope.sectorMsg = true;
				$scope.sectorError = true;
				$scope.sectorCorrect = false;
			}
			
			if(register.hasAgreed == undefined ){
				$scope.tmsg = "You must agree before saving.";
				$scope.termsError = true;
			}
		}
	}
	
	$scope.nameCheck = function (){
	
		if(isAlpha($scope.name)){
			$scope.nameError = false;
			$scope.nameCorrect = true;
			$scope.nameMsg = true;
			$scope.nmsg = "Looks good!";
		}
		else{
			$scope.nameError = true;
			$scope.nameCorrect = false;
			$scope.nameMsg = true;	
			$scope.nmsg = "Invalid name!";
		}
	};
	
	$scope.sectorCheck = function (){
			$scope.sectorError = false;
			$scope.sectorCorrect = true;
			$scope.sectorMsg = true;
			$scope.smsg = "Looks good!";
	};
	
	$scope.termsCheck = function (){
		$scope.termsError = false;
	};

	function isAlpha(str) {
		  return /^[a-zA-Z() ]+$/.test(str) && str !== undefined;
		}
 });
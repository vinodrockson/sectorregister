var app = angular.module('project', [ "ngRoute", "ngCookies" ]);

app.config(function($routeProvider, $locationProvider, $httpProvider) {
	$routeProvider.when(
			'/main',
			{	controller : 'RegisterController',
				templateUrl : 'views/register.html'
			}).otherwise({
				redirectTo : '/main'
			});	
});

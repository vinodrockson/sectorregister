package com.sector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SuperSector {

	@Id
	@GeneratedValue
	Long id;
	
	String sectorName;
	Long mainSectorId;
	String level;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSectorName() {
		return sectorName;
	}
	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	public Long getMainSectorId() {
		return mainSectorId;
	}
	public void setMainSectorId(Long mainSectorId) {
		this.mainSectorId = mainSectorId;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
}

package com.sector.models;

import java.util.List;

public class Sector {
	
	List<MainSector> mainSectors;
	List<SuperSector> superSectors;
	List<SubSector> subSectors;
	List<MicroSector> microSectors;
	public List<MainSector> getMainSectors() {
		return mainSectors;
	}
	public void setMainSectors(List<MainSector> mainSectors) {
		this.mainSectors = mainSectors;
	}
	public List<SuperSector> getSuperSectors() {
		return superSectors;
	}
	public void setSuperSectors(List<SuperSector> superSectors) {
		this.superSectors = superSectors;
	}
	public List<SubSector> getSubSectors() {
		return subSectors;
	}
	public void setSubSectors(List<SubSector> subSectors) {
		this.subSectors = subSectors;
	}
	public List<MicroSector> getMicroSectors() {
		return microSectors;
	}
	public void setMicroSectors(List<MicroSector> microSectors) {
		this.microSectors = microSectors;
	}
}

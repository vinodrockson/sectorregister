package com.sector.models;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.sector.utils.StringListConverter;

@Entity

public class Register {
	
	@Id
	@GeneratedValue
	Long id;
	
	String name;
	
	@Convert(converter = StringListConverter.class)
	List<String> sectorId;
	
	boolean hasAgreed;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<String> getSectorId() {
		return sectorId;
	}
	public void setSectorId(List<String> sectorId) {
		this.sectorId = sectorId;
	}
	public boolean isHasAgreed() {
		return hasAgreed;
	}
	public void setHasAgreed(boolean hasAgreed) {
		this.hasAgreed = hasAgreed;
	}

}

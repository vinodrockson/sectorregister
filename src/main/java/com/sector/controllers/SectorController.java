package com.sector.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sector.models.MainSector;
import com.sector.models.MicroSector;
import com.sector.models.Sector;
import com.sector.models.SubSector;
import com.sector.models.SuperSector;
import com.sector.repositories.MainSectorRespository;
import com.sector.repositories.MicroSectorRepository;
import com.sector.repositories.SubSectorRepository;
import com.sector.repositories.SuperSectorRepository;

@RestController
@RequestMapping("/rest/sector")
public class SectorController {
	
	@Autowired
	MainSectorRespository mainRespository;
	
	@Autowired
	SuperSectorRepository superRepository;
	
	@Autowired
	SubSectorRepository subRepository;
	
	@Autowired
	MicroSectorRepository microRepository;

	// To fetch all the pre-stored sector data from the repository
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ResponseStatus(HttpStatus.OK)
	public Sector getAllSectors() {
		List<MainSector> mainSectors = mainRespository.findAll();
		List<SuperSector> superSectors = superRepository.findAll();
		List<SubSector> subSectors = subRepository.findAll();
		List<MicroSector> microSectors = microRepository.findAll();
		
		Sector sector = new Sector();
		sector.setMainSectors(mainSectors);
		sector.setSuperSectors(superSectors);
		sector.setSubSectors(subSectors);
		sector.setMicroSectors(microSectors);
		return sector;
	}


}

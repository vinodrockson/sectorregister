package com.sector.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sector.models.Register;
import com.sector.repositories.RegisterRepository;

@RestController
@RequestMapping("/rest/register")
public class RegisterController {

	@Autowired
	RegisterRepository registerRepository;

	// To create new user data
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Register> registerUser(@RequestBody Register register) {
		if (register == null) {
			return new ResponseEntity<Register>(
					HttpStatus.UNPROCESSABLE_ENTITY);
		}
		Register createdRegister = registerRepository.saveAndFlush(register);
		return new ResponseEntity<Register>(createdRegister, HttpStatus.CREATED);
	}

	// To update new user data
	@RequestMapping(method = RequestMethod.PUT, value="/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Register> updateUser(@RequestBody Register updatedRegister, @PathVariable("id") Long id)	throws Exception {
		if (updatedRegister == null) {
			return new ResponseEntity<Register>(
					HttpStatus.UNPROCESSABLE_ENTITY);
		}

	   Register newRegister= registerRepository.getOne(id);
	   
		if (newRegister == null) {
			return new ResponseEntity<Register>(
					HttpStatus.NOT_FOUND);
		}
		
	   newRegister.setName(updatedRegister.getName());
	   newRegister.setSectorId(updatedRegister.getSectorId());
	   newRegister.setHasAgreed(updatedRegister.isHasAgreed());
	   newRegister= registerRepository.save(newRegister);
	   return new ResponseEntity<Register>(newRegister,HttpStatus.OK);
	}

}

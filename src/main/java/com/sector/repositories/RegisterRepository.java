package com.sector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sector.models.Register;

public interface RegisterRepository extends JpaRepository<Register, Long> {


}
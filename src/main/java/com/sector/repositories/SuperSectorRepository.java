package com.sector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sector.models.SuperSector;


public interface SuperSectorRepository extends JpaRepository<SuperSector, Long> {


}

package com.sector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sector.models.SubSector;

public interface SubSectorRepository extends JpaRepository<SubSector, Long> {


}

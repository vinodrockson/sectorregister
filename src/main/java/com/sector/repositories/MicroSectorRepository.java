package com.sector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sector.models.MicroSector;


public interface MicroSectorRepository extends JpaRepository<MicroSector, Long> {


}
package com.sector.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sector.models.MainSector;

public interface MainSectorRespository  extends JpaRepository<MainSector, Long> {


}

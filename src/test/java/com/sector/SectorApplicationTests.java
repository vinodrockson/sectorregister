package com.sector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.sector.models.Register;
import com.sector.models.Sector;
import com.sector.repositories.MicroSectorRepository;
import com.sector.repositories.RegisterRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { SectorApplication.class })
@ActiveProfiles("test")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class SectorApplicationTests {

	@Autowired
	MicroSectorRepository microRepository;
	
	@Autowired
	RegisterRepository registerRepository;
	
	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	private ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void checkMicroRepo() {
		assertEquals(false, microRepository.findById(53L).isEmpty());	
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetSectors() throws Exception {

		MvcResult result = mockMvc
				.perform(get("/rest/sector")).andExpect(status().isOk()).andReturn();

		Sector sectorData = mapper.readValue(result.getResponse()
				.getContentAsString(), Sector.class);
		
		assertEquals(sectorData.getMainSectors().size(), 3);
		assertEquals(sectorData.getMicroSectors().size(), 2);
		assertEquals(sectorData.getSubSectors().size(), 3);
		assertEquals(sectorData.getSuperSectors().size(), 5);
		assertEquals(sectorData.getSubSectors().get(2).getSectorName(), "Telecommunications");
		assertEquals(sectorData.getMicroSectors().get(0).getSubSectorId().intValue(), 97);
	}

	//If any error, run this test separately
	@Test
	@DatabaseSetup(value = "EmptyDatabase.xml")
	public void testRegisterUserData() throws Exception {
		
		Register register = new Register();
		register.setId(10L);
		register.setName("Tony Stark");
		List<String> secIds = new ArrayList<String>();
		secIds.add("98");
		secIds.add("13");
		register.setSectorId(secIds);
		register.setHasAgreed(true);

		MvcResult result = mockMvc
				.perform(
						post("/rest/register").contentType(
								MediaType.APPLICATION_JSON).content(
								mapper.writeValueAsString(register)))
				.andExpect(status().isCreated()).andReturn();
		
		Register resultRegister = mapper.readValue(result.getResponse()
				.getContentAsString(), Register.class);
		
		assertEquals(resultRegister.getId().longValue(), 1L);
	}
	}
